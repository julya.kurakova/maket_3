// Инициализация Swiper
const swiperSettings = {
    spaceBetween: 16,
    slidesPerView: 'auto',
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
   };

const mySwiper = new Swiper('.swiper', swiperSettings);

// Управление видимостью элементов
const flexItems = document.querySelectorAll('.swiper-slide');
const showHideButton = document.querySelector('.show-hide-button');
const showHideImage = document.querySelector('.show-hide-button__image');

let isHidden = true; //для отслеживания состояния видимости элементов
let numVisibleItems = 6; // Количество отображаемых элементов по умолчанию

const setNumVisibleItems = () => {   //задает количество видимых элементов по умолчанию.
    if (window.innerWidth >= 320 && window.innerWidth <= 767) {
        numVisibleItems = 11;
    } else if (window.innerWidth >= 768 && window.innerWidth <= 1119) {
        numVisibleItems = 6;
    } else {
        numVisibleItems = 8;
    }

    flexItems.forEach((item, index) => {
        item.style.display = index < numVisibleItems ? 'flex' : 'none';
    });
};

// Установить количество отображаемых элементов при загрузке и при изменении размера окна
window.addEventListener('load', setNumVisibleItems);
window.addEventListener('resize', setNumVisibleItems);

//Кнопка "Показать все"/"Cкрыть все"
showHideButton.addEventListener('click', function() {
    flexItems.forEach((item, index) => {
        if (index >= numVisibleItems) {
            item.style.display = isHidden ? 'flex' : 'none';
        }
    }); // при первом клике ishidden=true, значит элементы скрыты, а станут flex

    isHidden = !isHidden; // дальше ishidden = false, значит при следующем клике flex элементы cтанут none
    
    showHideButton.textContent = isHidden ? 'Показать все' : 'Скрыть все';

    const rotationValue = isHidden ? '0deg' : '180deg';
    showHideImage.style.transform = `rotate(${rotationValue})`;
});